package com.rezafaiz.spritesheetanimation;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class SpriteSheetAnimation extends Activity {

    GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gameView = new GameView(this);
        setContentView(gameView);
    }

    class GameView extends SurfaceView implements Runnable{

        Thread gameThread = null;
        SurfaceHolder ourHolder;
        volatile boolean playing;

        Canvas canvas;
        Paint paint;

        long fps;

        private long timeThisFrame;

        Bitmap bitmapBob, bobReversed, drawd;
        boolean isMoving = false;

        float walkSpeedPerSecond = 250;
        float bobXPosition = 10;

        private int frameWidth = 100;
        private int frameHeight = 50;

        private int frameCount = 5;
        private long lastFrameChangeTime = 0;

        private Rect frameToDraw = new Rect(
                0,
                0,
                frameWidth,
                frameHeight
        );

        private int currentFrame = 0;
        private int frameLengthInMilliseconds = 100;

        RectF whereToDraw = new RectF(
                bobXPosition, 0,
                bobXPosition + frameWidth,
                frameHeight
        );

        int width;
        boolean isReverse;

        public GameView (Context context){

            super(context);

            isReverse = false;

            ourHolder = getHolder();
            paint = new Paint();

            Matrix matrix = new Matrix();
            matrix.setScale(-1,1);

            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob300x104);

            bitmapBob = Bitmap.createScaledBitmap(bitmapBob,
                    frameWidth * frameCount,
                    frameHeight, false);
            bobReversed = Bitmap.createBitmap(bitmapBob,0,0,bitmapBob.getWidth(),bitmapBob.getHeight(),matrix,true);

            drawd = bitmapBob;

            Display display =((Activity) context).getWindowManager().getDefaultDisplay();
            width = display.getWidth();

        }

        public void getCurrentFrame(){
            long time = System.currentTimeMillis();
            if (isMoving){
                if (time > lastFrameChangeTime + frameLengthInMilliseconds){
                    lastFrameChangeTime = time;
                    currentFrame++;
                    if (currentFrame >= frameCount){
                        currentFrame = 0;
                    }
                }
            }

            frameToDraw.left = currentFrame * frameWidth;
            frameToDraw.right = frameToDraw.left + frameWidth;

        }

        @Override
        public void run(){

            while (playing){

                long startFrameTime = System.currentTimeMillis();
                update();

                // Draw the frame
                draw();

                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame >= 1) {
                    fps = 1000 / timeThisFrame;
                }

            }

        }

        public void update() {

            if(isMoving){
                bobXPosition = bobXPosition + (walkSpeedPerSecond / fps);

                if (bobXPosition + frameWidth >= width || bobXPosition <= 0) {
                    walkSpeedPerSecond = -walkSpeedPerSecond;
                    if (isReverse)
                        isReverse = false;
                    else
                        isReverse = true;
                }
            }

        }

        public void draw() {

            if (ourHolder.getSurface().isValid()) {
                canvas = ourHolder.lockCanvas();

                canvas.drawColor(Color.argb(255, 26, 128, 182));
                paint.setColor(Color.argb(255,  249, 129, 0));

                paint.setTextSize(45);

                canvas.drawText("FPS:" + fps, 20, 40, paint);
                //canvas.drawBitmap(bitmapBob, bobXPosition, 200, paint);

                whereToDraw.set((int)bobXPosition,
                        50,
                        (int)bobXPosition + frameWidth,
                        frameHeight + 100);

                getCurrentFrame();

                if (!isReverse){
                    canvas.drawBitmap(bitmapBob,
                        frameToDraw,
                        whereToDraw, paint);
                }
                else {
                    canvas.drawBitmap(bobReversed,
                            frameToDraw,
                            whereToDraw, paint);
                }



                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }

        }

        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                // Player has touched the screen
                case MotionEvent.ACTION_DOWN:

                    // Set isMoving so Bob is moved in the update method
                    isMoving = true;

                    break;

                // Player has removed finger from screen
                case MotionEvent.ACTION_UP:

                    // Set isMoving so Bob does not move
                    isMoving = false;

                    break;
            }
            return true;
        }

    }
    // This is the end of our GameView inner class

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the gameView resume method to execute
        gameView.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the gameView pause method to execute
        gameView.pause();
    }

}
